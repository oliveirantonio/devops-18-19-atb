1- no site hub.docker.com
——————

Fiz um “Create Repository” com o seguinte nome: ca5-part2

Obtive a seguinte tag:
To push a new tag to this repository,
docker push 1181691/ca5-part2:tagname

Link:
https://hub.docker.com/r/1181691/ca5-part2


2- no directório do “ca4”.
——————

Fui buscar/copiar o “Dockerfile”.
Fui buscar/copiar a pasta “war”.

Coloquei-o no directório “ca2” na sub-pasta “part2”


3- no ficheiro “Jenkins” (“ca2 -> part2”)
——————

Coloquei as seguintes linhas de código:

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: '1181691-bitbucket-credentials-ca5', url: 'https://oliveirantonio@bitbucket.org/oliveirantonio/devops-18-19-atb_v2'
            }
        }
        stage('Assembling') {
            steps {
                echo 'Assembling...'
		   sh 'cd ca2/part2; chmod 755 ./gradlew; ./gradlew clean assemble'
            }
        }
        stage('Testing') {
            steps {
                echo 'Testing...'
                sh 'cd ca2/part2; ./gradlew test'
                junit '**/test-results/test/*.xml'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca2/part2/build/libs/*'
            }
        }
	 stage ('Docker Image') {
           steps {
               echo 'Building and pushing image...'
               script {
                   def errai_demo = docker.build("oliveirantonio/ca5-part2:${env.BUILD_ID}",'./ca2/part2')
                   docker.withRegistry('https://registry.hub.docker.com', '1181691-bitbucket-credentials-ca5') {
                       errai_demo.push('latest')
                   }
               }
           }
       }
    }
}


4- no ficheiro “Dockerfile” (“ca2 -> part2”)
——————

Ficheiro Dockerfile:
FROM ubuntu

RUN apt-get update && \
  apt-get install -y openjdk-8-jdk-headless && \
  apt-get install unzip -y && \
  apt-get install wget -y

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app/

RUN wget http://download.jboss.org/wildfly/15.0.1.Final/wildfly-15.0.1.Final.tar.gz && \
  tar -xvzf wildfly-15.0.1.Final.tar.gz && \
  sed -i 's#<inet-address value="${jboss\.bind\.address:127\.0\.0\.1}"/>#<any-address/>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
  sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
  sed -i 's#<password>sa</password>#<password></password>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml

COPY war/errai-demonstration-gradle.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/

CMD /usr/src/app/wildfly-15.0.1.Final/bin/standalone.sh


Código que inseri no “Dockerfile”:
war/errai-demonstration-gradle.war

Que vai buscar na pasta “war” o ficheiro “war/errai-demonstration-gradle.war”. 


5- no Jenkins (localhost). Pipeline “Pipeline_ca5_part2”
——————

Este Pipeline tem as seguinte características:

Repository URL: https://oliveirantonio@bitbucket.org/oliveirantonio/devops-18-19-atb_v2

Credentials: oliveirantonio/*****(credentials-ca5)


6- no Jenkins (localhost). Pipeline “Pipeline_ca5_part2”
——————

Fiz correr o “Buid Now” na Pipeline “Pipeline_ca5_part2”.

Obtive estas respostas:

	Declarative: Checkout SCM - Success.

	Checkout - Success.

	Assembling - Success.

	Testing - Success.

	Archiving - Success.

	Docker Image - Failed.


7- no Jenkins (localhost). no Console Output
——————

Obtive esta informação:
Step 6/7 : COPY war/errai-demonstration-gradle.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/
COPY failed: stat /mnt/sda1/var/lib/docker/tmp/docker-builder638016713/war/errai-demonstration-gradle.war: no such file or directory









