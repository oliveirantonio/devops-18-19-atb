
1- Abri o Docker.
—————

Docker Quickstar Terminal.


2- No TERMINAL.
—————

inseri esta linha de comando:
docker run -u root --rm -d -p 8080:8080 -p 50000:50000 -v $HOME/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock jenkinsci/blueocean3- No Browser.
—————

Fui a este link:
http://192.168.99.100:8080/login?from=%2Fjenkins

4- No TERMINAL.
—————

fui ao jenkins/secrets

corri este comando:
- cat initialAdminPassword

Deu-me a password:
e0423c597cb34c81968d592bbb424c7d


5- No Browser.
—————

Fui a este link:
http://192.168.99.100:8080/login?from=%2Fjenkins

inseri a password: e0423c597cb34c81968d592bbb424c7d

Depois acedi:
“Install suggested Plugin”


6- No Browser.
—————

Fui para a página “Getting Started”.
Fui direccionado para outra página “Create First Admin User”.

Username: 1181691	
Password: Switch_79porto	
Confirm password: Switch_79porto	
Full name: 1181691	
E-mail address:	 1181691@isep.ipp.pt


7- no Bitbucket
—————

Criei a um novo repositório (https://oliveirantonio@bitbucket.org/oliveirantonio/devops-18-19-atb_v2)

Nesse repositório fiz um commit e push da seguinte pasta e sub-pastas (respectivos ficheiros): ca2/part1/luisnogueira-gradle_basic_demo-fef780d15427/…


8- no Directório (…/luisnogueira-gradle_basic_demo-fef780d15427/…)
—————

coloquei o ficheiro “Jenkinsfile”.


9- no Jenkins (localhost)
—————

Criei uma Credencial com os seguintes dados:
Username: oliveirantonio
password: **********
ID: 1181691-bitbucket-credentials-ca5
Description: credentials-ca5


10- no Jenkins (localhost)
—————

Criei a Pipeline “Pipeline_Script_Path” com os seguintes dados:

Repository URL: https://oliveirantonio@bitbucket.org/oliveirantonio/devops-18-19-atb_v2

Credentials: oliveirantonio/***** (credentials-ca5)

Script Path: ca2/part1/luisnogueira-gradle_basic_demo-fef780d15427/Jenkinsfile


11- no ficheiro Jenkins
—————

Editei o ficheiro com os seguintes dados:

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: '1181691-bitbucket-credentials-ca5', url: 'https://oliveirantonio@bitbucket.org/oliveirantonio/devops-18-19-atb_v2'
            }
        }
        stage('Assembling') {
            steps {
                echo 'Assembling...'
		   sh 'cd ca2/part1/luisnogueira-gradle_basic_demo-fef780d15427; chmod 755 ./gradlew; ./gradlew clean assemble'
            }
        }
        stage('Testing') {
            steps {
                echo 'Testing...'
                sh 'cd ca2/part1/luisnogueira-gradle_basic_demo-fef780d15427; ./gradlew test'
                junit '**/test-results/test/*.xml'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'ca2/part1/luisnogueira-gradle_basic_demo-fef780d15427/build/distributions/*'
            }
        }
    }
}


12- no Jenkins (localhost)
—————

Fiz correr o “Build Now” da Pipeline “Pipeline_Script_Path”.

Obtive este resultado:
	Declarative: Checkout SCM - 3s
	Checkout: 2s
	Assembling: 2min 6s
	Testing: 18s
	Archiving: 1s

Tudo isto num fundo verde, a dar um feedback positivo.





