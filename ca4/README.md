
1 - No TERMINAL.
_________

- git add -A

- git commit -a -m “ca4 first commit”

- git push origin master


2- No TERMINAL.
_________

- docker info

Obtive esta informação:
MacBook-Pro-de-Antonio-3:ca4 antoniooliveira$ docker info
Containers: 0
 Running: 0
 Paused: 0
 Stopped: 0
Images: 0
Server Version: 18.09.5
Storage Driver: overlay2
 Backing Filesystem: extfs
 Supports d_type: true
 Native Overlay Diff: true
Logging Driver: json-file
Cgroup Driver: cgroupfs
Plugins:
 Volume: local
 Network: bridge host macvlan null overlay
 Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
Swarm: inactive
Runtimes: runc
Default Runtime: runc
Init Binary: docker-init
containerd version: bb71b10fd8f58240ca47fbb579b9d1028eea7c84
runc version: 2b18fe1d885ee5083ef9f0838fee39b62d653e30
init version: fec3683
Security Options:
 seccomp
  Profile: default
Kernel Version: 4.14.111-boot2docker
Operating System: Boot2Docker 18.09.5 (TCL 8.2.1)
OSType: linux
Architecture: x86_64
CPUs: 1
Total Memory: 1.951GiB
Name: default
ID: O4EU:KRPG:53IF:G6AG:Q4U2:5FUL:ZWCO:L37W:HUZS:2KKE:FWNO:ZOQZ
Docker Root Dir: /mnt/sda1/var/lib/docker
Debug Mode (client): false
Debug Mode (server): false
Registry: https://index.docker.io/v1/
Labels:
 provider=virtualbox
Experimental: false
Insecure Registries:
 127.0.0.0/8
Live Restore Enabled: false


3- No Browser.
_________

Download do link (https://github.com/atb/docker- compose- demo)


4- No Directório (ca4).
_________

colocar/copiar:
- docker-compose.yml
- readme.md
- pasta “db”
- pasta “web”

5- No Directório (ca4).
_________

colocar/copiar da projecto anterior (ca3) o ficheiro:
- errai-demonstration-gradle-master.war

Este ficheiro foi colocado na sub-pasta “war”, que está dentro da pasta “web”.


6- No TERMINAL.
_________

- docker-compose up


7- No Directório. dentro da pasta “web”.
_________

- Inseri o seguinte código no ficheiro “Dockerfile”:

RUN wget http://download.jboss.org/wildfly/15.0.1.Final/wildfly-15.0.1.Final.tar.gz && \
  tar -xvzf wildfly-15.0.1.Final.tar.gz && \
  sed -i 's#<inet-address value="${jboss\.bind\.address:127\.0\.0\.1}"/>#<any-address/>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
  sed -i 's#<connection-url>jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#<connection-url>jdbc:h2:file://usr/src/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE</connection-url>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml && \
  sed -i 's#<password>sa</password>#<password></password>#g' /usr/src/app/wildfly-15.0.1.Final/standalone/configuration/standalone.xml

COPY war/errai-demonstration-gradle.war /usr/src/app/wildfly-15.0.1.Final/standalone/deployments/

8- No Directório. dentro da pasta “ca4”.
_________

- Inseri o seguinte código no ficheiro “docker-compose.yml”:

version: '3'
services:
  db:
    build: db
    ports:
      - "8082:8082"
      - "9092:9092"
    volumes:
      - ./data:/usr/src/data

  web:
    build: web
    ports:
      - "8080:8080"

8- No TERMINAL.
_________

- docker-compose build


9- No TERMINAL.
_________

- docker-compose up


10- No browser.
_________

http://192.168.99.100:8080/errai-demonstration-gradle/ContactListPage

Surge a página para inserir os dados (nome e mail).


11- No directório: web
_________

No ficheiro “Dockerfile”, alterei/inseri parte na linha de texto:
<connection-url>jdbc:h2:tcp://db:9092//usr/src/data

Objectivo:
Fazer a persistência na base de dados.

12- No TERMINAL.
_________

- docker-compose build
- docker-compose up


13- No Browser.
_________

Na página:http://192.168.99.100:8082/login.jsp?jsessionid=15c8e3dad6ce1eee736c899ebfa07d1d


No campo “JDBC URL:”, inseri este link:
jdbc:h2:tcp://db:9092//usr/src/data/data;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=FALSE


14- No Browser.
_________

Na página:
http://192.168.99.100:8080/errai-demonstration-gradle/ContactListPage

Inseri os dados para persistir na base de dados.


15- No directório.
_________

Na pasta “data”: surge o ficheiro “data.mv.db” que faz a persistência da informação.


_________
DOCKER - TERMINAL
_________


16- No TERMINAL.
_________

- docker login

Username: 1181691
Password:


17- No TERMINAL.
_________

- docker images

REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
ca4_web             latest              56978df22853        About an hour ago
…


18- No TERMINAL.
_________


- docker tag 56978df22853 1181691/ca4_web:web

- docker push 1181691/ca4_web

Obtive esta informação:
The push refers to repository [docker.io/1181691/ca4_web]
65eb71da4077: Pushed 
002c518fae8a: Pushed 
ca6e05b1bb01: Pushed 
544bdf259bcb: Pushed 
7660ded5319c: Mounted from library/ubuntu 
94e5c4ea5da6: Mounted from library/ubuntu 
5d74a98c48bc: Mounted from library/ubuntu 
604cbde1a4c8: Mounted from library/ubuntu 
latest: digest: sha256:19b79ed3c7659eaea30f523d74da914656d906fcf3d82bd6b0e6b478ab43a05e size: 1994
65eb71da4077: Layer already exists 
002c518fae8a: Layer already exists 
ca6e05b1bb01: Layer already exists 
544bdf259bcb: Layer already exists 
7660ded5319c: Layer already exists 
94e5c4ea5da6: Layer already exists 
5d74a98c48bc: Layer already exists 
604cbde1a4c8: Layer already exists 
web: digest: sha256:19b79ed3c7659eaea30f523d74da914656d906fcf3d82bd6b0e6b478ab43a05e size: 1994


19- No TERMINAL.
_________

- docker tag 56978df22853 1181691/ca4_db:db

- docker push 1181691/ca4_db

Obtive esta informação:
The push refers to repository [docker.io/1181691/ca4_db]
65eb71da4077: Mounted from 1181691/ca4_web 
002c518fae8a: Mounted from 1181691/ca4_web 
ca6e05b1bb01: Mounted from 1181691/ca4_web 
544bdf259bcb: Mounted from 1181691/ca4_web 
7660ded5319c: Mounted from 1181691/ca4_web 
94e5c4ea5da6: Mounted from 1181691/ca4_web 
5d74a98c48bc: Mounted from 1181691/ca4_web 
604cbde1a4c8: Mounted from 1181691/ca4_web 
db: digest: sha256:19b79ed3c7659eaea30f523d74da914656d906fcf3d82bd6b0e6b478ab43a05e size: 1994


————————
DOCKER - LINKS DO REPOSITORIO:
————————

https://hub.docker.com/r/1181691/ca4_db

https://hub.docker.com/r/1181691/ca4_web
