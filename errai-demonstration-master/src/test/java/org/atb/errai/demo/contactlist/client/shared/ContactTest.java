package org.atb.errai.demo.contactlist.client.shared;

import org.junit.Test;

import static org.junit.Assert.*;

public class ContactTest {

    @Test
    public void validateEmailValidMail() {
        //Arrange
        Contact mContact = new Contact();
        String emailValid = "oli@mail.pt";
        String expectedResult = "oli@mail.pt";


        //Act
        String result = mContact.validateEmail(emailValid);

        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void notValidateEmail() {
        //Arrange
        Contact mContact = new Contact();
        String emailNotValid = "oli.mail.pt";
        String expectedResult = "* invalid email *";

        //Act
        String result = mContact.validateEmail(emailNotValid);

        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void isEmptyEmail() {
        //Arrange
        Contact mContact = new Contact();
        String emailNotValid = "";
        String expectedResult = "* invalid email *";

        //Act
        String result = mContact.validateEmail(emailNotValid);

        //Assert
        assertEquals(expectedResult, result);
    }

    @Test
    public void getName() {
        Contact contact = new Contact("António", "oliveirantonio@gmail.com","933285910","4150-345");
        contact.setName("António");
        String expectedResult = contact.getName();
        String result = "António";
        assertEquals(result,expectedResult);
    }

    @Test
    public void getEmail() {
        Contact contact = new Contact("António", "oliveirantonio@gmail.com","933285910","4150-345");
        contact.setEmail("oliveirantonio@gmail.com");
        String expectedResult = contact.getEmail();
        String result = "oliveirantonio@gmail.com";
        assertEquals(result,expectedResult);
    }

    @Test
    public void getPhone() {
        Contact contact = new Contact("António", "oliveirantonio@gmail.com","933285910","4150-345");
        contact.setPhone("933285910");
        String expectedResult = contact.getPhone();
        String result = "933285910";
        assertEquals(result,expectedResult);
    }

    @Test
    public void getPostalCode() {
        Contact contact = new Contact("António", "oliveirantonio@gmail.com","933285910","4150-345");
        contact.setPostalCode("4150-345");
        String expectedResult = contact.getPostalCode();
        String result = "4150-345";
        assertEquals(result,expectedResult);
    }
}