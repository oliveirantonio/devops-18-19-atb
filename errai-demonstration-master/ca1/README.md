1- No TERMINAL.

- cd devops-18-19-atb/errai-demonstration-master

Aceder à pasta do repositorio

---
2- No TERMINAL.
- git tag -a v1.2.0 -m "my version v.1.2.0"

---
3- No IntelliJ IDEA.

Criei um novo campo (postal code) para além do que já existia (phone number). Ficheiros onde fiz as alterações:
- Contact (que fica na pasta “shared”).
- NewContactPage.html (fica na pasta “local”).
- NewContactPage (class que fica na pasta “local”).
- ContactListPage.html (fica na pasta “local”).
- ContactDisplay (class que fica na pasta “local”).
---
4- No TERMINAL. (IntelliJ IDEA)

- git commit -a -m “commit”

---
5- No TERMINAL.
- git push origin master

---
6- No TERMINAL.
- git tag -a v1.3.0 -m "my version v1.3.0"

---
7- No TERMINAL.
- git remote -v

Obtive esta informação:
origin	https://oliveirantonio@bitbucket.org/oliveirantonio/devops-18-19-atb.git (fetch)
origin	https://oliveirantonio@bitbucket.org/oliveirantonio/devops-18-19-atb.git (push)
---

8- No TERMINAL.
- git checkout -b fix-invalid-email

Obtive este informação:

Switched to a new branch 'fix-invalid-email'

---

9- No TERMINAL.
- git push origin fix-invalid-email

Criou um novo branch.

---

10- No IntelliJ IDEA.

Na classe Contact (no directório shared) inseri código java para fazer uma simples validação para o campo email, com os devidos testes.

---

11- No TERMINAL.
- git commit -a -m “commit”

---
12- No TERMINAL.
- git push origin fix-invalid-email

Obtive esta informação:

Total 0 (delta 0), reused 0 (delta 0)
remote: 
remote: Create pull request for fix-invalid-email:
remote:   https://bitbucket.org/oliveirantonio/devops-18-19-atb/pull-requests/new?source=fix-invalid-email&t=1
remote: 
To https://bitbucket.org/oliveirantonio/devops-18-19-atb.git
 * [new branch]      fix-invalid-email -> fix-invalid-email
MBPdeAntonio3:errai-demonstration-master antoniooliveira$
---
13- No TERMINAL.

- git checkout master

Obtive esta informação:

Switched to branch 'master'
Your branch is up to date with 'origin/master'.
---
14- No TERMINAL.
- git pull origin master

Obtive esta informação:

From https://bitbucket.org/oliveirantonio/devops-18-19-atb
 * branch            master     -> FETCH_HEAD
Already up to date.
---
15- No TERMINAL.
- git merge fix-invalid-email

Obtive esta informação:

Updating 7d2ef87..3ff8463
Fast-forward
 .../contactlist/client/local/ContactListPage.html  | 18 ++++++++--------
 .../demo/contactlist/client/shared/Contact.java    | 25 ++++++++++++++++------
 2 files changed, 27 insertions(+), 16 deletions(-)
---
16- No TERMINAL.
- git push origin master.

Obtive esta informação:
Total 0 (delta 0), reused 0 (delta 0)
To https://bitbucket.org/oliveirantonio/devops-18-19-atb.git
   7d2ef87..3ff8463  master -> master
---

17- No TERMINAL.
- git commit -a -m "commit"

Obtive esta informação:

[master ee259c2] commit
 1 file changed, 1 insertion(+)
---
18- No TERMINAL.
- git push origin master

Obtive esta informação:

Enumerating objects: 27, done.
Counting objects: 100% (27/27), done.
Delta compression using up to 4 threads
Compressing objects: 100% (6/6), done.
Writing objects: 100% (14/14), 849 bytes | 23.00 KiB/s, done.
Total 14 (delta 3), reused 0 (delta 0)
To https://bitbucket.org/oliveirantonio/devops-18-19-atb.git
   bbb88ab..ee259c2  master -> master
---
