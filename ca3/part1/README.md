Virtualization with Vagrant
—————————
—————————

1- No meu computador.
—————————

Criei a pasta “ca3”, com duas sub-pastas lá dentro (“part1” e “part2”).

A pasta ca3 esta no seguinte path:
/Users/antoniooliveira/devops-18-19-atb/ca3

2- No TERMINAL, fiz:
—————————

- git add -A
- git commit -a -m “createFolderCa3”
- git push origin master

Obtive esta informação:
2 files changed, 6 insertions(+), 5 deletions(-)
 create mode 100644 ca3/part1/README.md
MacBook-Pro-de-Antonio-3:ca3 antoniooliveira$ git push origin master
Enumerating objects: 16, done.
Counting objects: 100% (16/16), done.
Delta compression using up to 4 threads
Compressing objects: 100% (8/8), done.
Writing objects: 100% (10/10), 728 bytes | 6.00 KiB/s, done.
Total 10 (delta 6), reused 0 (delta 0)
To https://bitbucket.org/oliveirantonio/devops-18-19-atb.git
   2371ecd..9b0b4b8  master -> master

3- No BITBUCKET
—————————

Fui buscar o path para fazer clone do projecto:
git clone https://oliveirantonio@bitbucket.org/oliveirantonio/devops-18-19-atb.git

4- Na Máquina Virtual inseri o comando/path para clonar/:
—————————

git clone https://oliveirantonio@bitbucket.org/oliveirantonio/devops-18-19-atb.git

5- Na Máquina Virtual inseri este comando, para instalar o Maven.
—————————

- sudo apt install maven

6- Na Máquina Virtual. inseri este comando.
—————————

- mvn gwt:run

Deu um erro: “BUILD FAILURE”.

7- Na Máquina Virtual.
—————————

Fui ao directório onde está “devops-18-errai-demonstration-master”.
corri este comando:

- mvn gwt:run

Deu um erro: “BUILD FAILURE”.

8- Na Máquina Virtual.
—————————

fui ao directório onde estava a pasta ca2/part1/luisnogueira-gradlew_basic_demo_fef780d15427.

Fiz correr este comando, mas não fez sentido:

- gradle run

Mas tinha de fazer antes este comando:

- sudo apt install gradle

9- Na Máquina Virtual.
—————————

Fiz correr este comando:

- gradle run

Mas deu Erro.

10- Na Máquina Virtual.
————————-

Fui editar o ficheiro “build.gradle”

- nano build.gradle

comentei isto:
//task Zip(type: Zip) {
//	from ‘src’
//	into ‘backuotwo’
//	exclude ‘**/*.md’
//}

11- Na Máquina Virtual.
————————-

- gradle run

Com sucesso.


12- Na Máquina Virtual.
————————-

- gradle runServer


13- No Terminal.
————————-

fui ao directório onde estava a pasta ca2/part1/luisnogueira-gradlew_basic_demo_fef780d15427.

- gradle runServer

 

14- No Terminal.
————————-

Abri outra janela. Escrevi este comando:

- gradle runClient

E surgiu uma janela de chat…

15- ficheiro “build.gradle”
————————-

Na pasta onde está o ficheiro “build.gradle”, com um editor de texto escrevi
isto (192.168.56.100):

task runClient(type:JavaExec, dependsOn: classes){
    classpath = sourceSets.main.runtimeClasspath

    main = 'basic_demo.ChatClientApp'

    args ’192.168.56.100’, '59001'
}

16- No Terminal.
————————-

- ./gradlew runClient

obtive esta resposta:
- > Task :compileJava UP-TO-DATE
> Task :processResources UP-TO-DATE
> Task :classes UP-TO-DATE
> Task :runClient

17- No Terminal.
————————-

- git add -A
- git commit -a -m “CA3-FinalReadMePart1”
- git push origin master








