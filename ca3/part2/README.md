1- No TERMINAL, fiz:
—————————

fui ao directório (ca3/part2) e pus estes comandos:

- mkdir vagrant-project-1
- cd vagrant-project-1
- vagrant init envimation/ubuntu-xenial
- vagrant box add envimation/ubuntu-xenial http://www.dei.isep.ipp.pt/~alex/publico/ubuntu-xenial-virtualbox.box


2- No TERMINAL, fiz:
—————————

- vagrant up

e deu, em parte, esta informação:
==> default: Mounting shared folders...
    default: /vagrant => /Users/antoniooliveira/devops-18-19-atb/ca3/part2/vagrant-project-1

3- No TERMINAL, fiz:
—————————

Para aceder à máquina virtual:
- vagrant ssh

e deu, em parte, esta informação:
 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage
vagrant@base-debootstrap:~$ 

4- No directório/pasta (onde está o ficheiro “Vagrantfile”), fiz:
—————————

Descomentei as linhas de comandos, exemplo parcial:
config.vm.provision "shell", inline: <<-SHELL
     apt-get update
     apt-get install -y apache2
   SHELL
end
#

5- No TERMINAL, fiz:
—————————

- vagrant reload --provision

e deu esta informação:
1) en1: Wi-Fi (AirPort)
2) en0: Ethernet
3) p2p0

Which interface should the network bridge to? 

Optei pela opção “3”.

Depois esta resposta com vários “default:”, ex:
 …
 default: Enabling conf serve-cgi-bin.
 default: Enabling site 000-default.
 default: Processing triggers for libc-bin (2.23-0ubuntu10) ...
 default: Processing triggers for systemd (229-4ubuntu21) ...

6- Na Máquina Virtual, aconteceu o seguinte:
—————————

O “vagrant-project-1_default_15547…” ficou no estado Running.


7- No TERMINAL, fiz:
—————————

- vagrant halt

O “vagrant-project-1_default_15547…” ficou no estado Powered Off.


Part 2 class assignment
—————————
—————————

8- No Github, saquei:
—————————

fui ao link: https://github.com/atb/vagrant-multi-demo

coloquei a pasta no directório “part2”.


9- No TERMINAL, fiz:
—————————

- ./gradlew provision

e deu esta resposta:
BUILD SUCCESSFUL in 1m 6s
1 actionable task: 1 executed

10- No TERMINAL, fiz:
—————————

- ./gradlew build

e deu esta resposta:
BUILD FAILED in 5m 9s
5 actionable tasks: 5 executed

11- No directório onde esta o projecto (vagrant-multi-demo-master):
—————————

copiei o ficheiro “Vagrantfile”.

12- No directório onde esta o projecto (errai-demonstration-gradle-master):
—————————

Colei o ficheiro “Vagrantfile” no projecto (errai-demonstration-gradle-master).

13- No TERMINAL.
—————————

Acedendo ao projecto (errai-demonstration-gradle-master):
- vagrant up

e deu esta resposta:
    …
    db: .
    db: .....                         100%  523K=4.0s
    db: 
    db: 2019-04-08 16:49:17 (529 KB/s) - ‘h2-1.4.199.jar’ saved [2178179/2178179]
==> db: Running provisioner: shell...
    db: Running: inline script

14- No Browser:
—————————

fui ao link: http://192.168.33.11:8082

15- No Browser:
—————————

No JBDC: jdbc:h2:tcp://192.168.33.11:9092/~/test

Entrei na BD e tive acesso às tabelas.

16- No INTELLIJ (errai-demonstration-gradle-master):
—————————

No terminal:
- ./gradlew clean (com sucesso)
- ./gradlew provision (com sucesso)
- ./gradlew build

Resultado, deu erro quando fiz o build.

17- No directório
—————————

Fui ao projecto anterior (no ca2) e fiz copy/paste dos ficheiros/pasta “errai-demonstration-gradle-master.war” para dentro da pasta “deployments” que existe no projecto “errai-demonstration-gradle-master” que está no “ca3”.

18- no TERMINAL.
—————————

Fiz correr o comando:
- ./gradlew startWildfly

Obtive esta resposta:
BUILD FAILED in 4m 58s
5 actionable tasks: 1 executed, 4 up-to-date

19- no TERMINAL.
—————————

- ./gradlew clean (com sucesso)
- ./gradlew provision (com sucesso)

- ./gradlew build 
Obtive esta resposta:
BUILD FAILED in 6m 31s
5 actionable tasks: 5 executed


20- No INTELLIJ.
—————————

Fui à pasta das “Tasks” e depois à sub-pasta “build”.
Fiz clique no “build”.

Obtive esta resposta:
BUILD SUCCESSFUL in 4m 15s
8 actionable tasks: 6 executed, 2 up-to-date
18:31:54: Task execution finished 'build'.

21- No INTELLIJ.
—————————

Fui à pasta das “Tasks” e depois à sub-pasta “wildfly”:
Fiz clique no “starWildfly”.

Ficou duas horas a processar e interrompi.

Obtive esta resposta:
BUILD FAILED in 2h 8m 8s
8 actionable tasks: 1 executed, 7 up-to-date
Process 'command '/Users/antoniooliveira/devops-18-19-atb/ca3/part2/errai-demonstration-gradle-master/wildfly/bin/standalone.sh'' finished with non-zero exit value 143
11:36:12: Task execution finished 'startWildfly'.

22- no TERMINAL.
—————————

- ./gradlew startWildfly

Obtive esta resposta:
BUILD FAILED in 2m 2s
5 actionable tasks: 1 executed, 4 up-to-date

23- Os dois ficheiros “Vagrantfile”
—————————

A) Um que está no: … “ca3” e depois na “part2” e dentro do projecto “errai-demonstration-gradle-master”.

B) Outro que está no: … “ca3” e depois na “part2” e dentro do projecto  “vagrant-project-1”

Copiei parte do ficheiro “Vagrantfile” que está em B:
# Share an additional folder to the guest VM. The first argument is
  # the path on the host to the actual folder. The second argument is
  # the path on the guest to mount the folder. And the optional third
  # argument is a set of non-required options.
    config.vm.synced_folder "/Users/antoniooliveira/devops-18-19-atb/ca3/part2/vagrant-project-1/data", "/vagrant_data"

Para o ficheiro do A.

No ficheiro do A mudei o port 8080 para 8081:
# We want to access wildfly from the host using port 8080
    web.vm.network "forwarded_port", guest: 8081, host: 8081

24- no TERMINAL.
—————————

- vagrant up

deu este erro ou resposta:
Timed out while waiting for the machine to boot. This means that
Vagrant was unable to communicate with the guest machine within
the configured ("config.vm.boot_timeout" value) time period.

If you look above, you should be able to see the error(s) that
Vagrant had when attempting to connect to the machine. These errors
are usually good hints as to what may be wrong.

If you're using a custom box, make sure that networking is properly
working and you're able to connect to the machine. It is a common
problem that networking isn't setup properly in these boxes.
Verify that authentication configurations are also setup properly,
as well.

If the box appears to be booting properly, you may want to increase
the timeout ("config.vm.boot_timeout") value.

25- na Máquina Virtual.
—————————

Para tentar contornar/resolver o problema (passo 24):

1. Fui ligar primeiro o “…gradle-master_db_…”
2. Depois fui ligar o “…gradle-master_web_…”


MUDANÇA DE ABORDAGEM
—————————

25- No Directório.
—————————

No directório onde está o ficheiro “errai-demonstration-gradle-master.war”:
/Users/antoniooliveira/devops-18-19-atb/ca3/part2/errai-demonstration-gradle-master/build/libs

Copiei o ficheiro para onde está o ficheiro “Vagrantfile”.

E alterei o nome do ficheiro para “errai-demonstration-gradle.war”.

Para o ficheiro “Vagrantfile” aceder ao “errai-demonstration-gradle.war”.

26- no TERMINAL.
—————————

Onde está o directório do projecto:

- vagrant up

ligou as máquinas virtuais.


27- no Browser:
—————————

http://192.168.33.10:8080/errai-demonstration-gradle/ (onde insiro os dados)

http://192.168.33.11:8082 (acesso à base de dados)


28- no Ficheiro “Vagrantfile”
—————————

Alterei o path onde o ficheiro, com os dados gravados, vai ser partilhado no config.vm.synced_folder:

config.vm.synced_folder "/Users/antoniooliveira/devops-18-19-atb/ca3/part2/errai-demonstration-gradle-master/data", "/vagrant_data"


29- no Directório (onde está o projecto)
—————————

config.vm.synced_folder "/Users/antoniooliveira/devops-18-19-atb/ca3/part2/errai-demonstration-gradle-master/data", "/vagrant_data"

Indiquei a pasta que criei “vagrant” (no projecto).

30- no TERMINAL
—————————

Vai buscar as indicações do ficheiro “Vagrantfile” e construir.

- vagrant reload —-provision

31- 
—————————



