
1- Abri o Docker.
—————

Docker Quickstar Terminal.


2- No TERMINAL.
—————

inseri esta linha de comando:
docker run -u root --rm -d -p 8080:8080 -p 50000:50000 -v $HOME/jenkins:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock jenkinsci/blueocean3- No Browser.
—————

Fui a este link:
http://192.168.99.100:8080/login?from=%2Fjenkins

4- No TERMINAL.
—————

fui ao jenkins/secrets

corri este comando:
- cat initialAdminPassword

Deu-me a password:
e0423c597cb34c81968d592bbb424c7d


5- No Browser.
—————

Fui a este link:
http://192.168.99.100:8080/login?from=%2Fjenkins

inseri a password: e0423c597cb34c81968d592bbb424c7d

Depois acedi:
“Install suggested Plugin”


6- No Browser.
—————

Fui para a página “Getting Started”.
Fui direccionado para outra página “Create First Admin User”.

Username: 1181691	
Password: Switch_79porto	
Confirm password: Switch_79porto	
Full name: 1181691	
E-mail address:	 1181691@isep.ipp.pt


7- No Browser.
—————

Jenkins URL: http://192.168.99.100:8080/


xxxxx

8- No Browser.
—————


- Pipeline Pipeline_A

pipeline {
    agent any
    stages {
        stage('Build') {
            steps {
                echo 'Building...'
                script {
                    if (isUnix())
                        sh 'java -version'
                    else
                        bat 'java -version'
                }
            }
        }
        stage('Test') {
            steps {
                echo 'Testing...'
            }
        }
    }
}

xxxxx


8- No Browser.
—————

Configuration page of the Job.

Na “Pipeline_B” inseri este código:

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/luisnogueira/gradle_basic_demo'
            }
        }
        stage('Build') {
            steps {
                echo 'Building...'
                sh './gradlew clean build'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }
    }
}


9- No Browser.
—————

- Tive este feedback:

Pipeline example

pipeline { 
    agent any 

    stages { 
        stage('Checkout') { 
            steps { 
                echo 'Checking out...' 
                git 'https://bitbucket.org/luisnogueira/gradle_basic_demo' 
            } 
        } 
        stage('Build') { 
            steps { 
                echo 'Building...' 
                sh './gradlew clean build' 
            } 
        } 
        stage('Archiving') { 
            steps { 
                echo 'Archiving...' 
                archiveArtifacts 'build/distributions/*' 
            } 
        } 
    } 
}


10- No Browser.
—————

Fiz click na opção “Build Now”.


11- No Browser.
—————

Na “Console Output”.

- Tive este feedback:

Started by user 1181691
Running in Durability level: MAX_SURVIVABILITY
[Pipeline] Start of Pipeline
[Pipeline] End of Pipeline
Finished: SUCCESS


12- No Browser. No Repositório “luisnogueira”.
—————

Fui a este link:
https://bitbucket.org/luisnogueira/gradle_basic_demo/src/master/

Fui ao “fork” no visto/opção “privado”.

13- no meu Desktop.
—————
Criei a pasta “repobit”.


13- No Terminal.
—————

Fiz um clone, na pasta “repobit”:
git clone https://oliveirantonio@bitbucket.org/oliveirantonio/gradle_basic_demoo.git


14- Na pasta “repobit”
—————

- Criei o file “jenkinsfile” e copiei este código:

pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git 'https://bitbucket.org/luisnogueira/gradle_basic_demo'
            }
        }
        stage('Build') {
            steps {
                echo 'Building...'
                sh './gradlew clean build'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }
    }
}

15- No browser do Jenkins.
—————

Na Credential:
http://192.168.99.100:8080/credentials/store/system/domain/_/newCredentials

Username: 1181691
Password: Switch_79porto
ID: 1181691-bitbucket-credentials-ca5
Description: credentials-ca5

16- No file “jenkinsfile”.
—————

No file “jenkinsfile” alterei esta linha:
git credentialsId: '1181691-bitbucket-credentials-ca5', url:

File jenkinsfile total:
pipeline {
    agent any

    stages {
        stage('Checkout') {
            steps {
                echo 'Checking out...'
                git credentialsId: '1181691-bitbucket-credentials-ca5', url: 'https://bitbucket.org/atb/gradle_basic_demo'
            }
        }
        stage('Build') {
            steps {
                echo 'Building...'
                sh './gradlew clean build'
            }
        }
        stage('Archiving') {
            steps {
                echo 'Archiving...'
                archiveArtifacts 'build/distributions/*'
            }
        }
    }
}

	
17- No TERMINAL.
—————

- git add -A

- git commit -a -m “ca5ChangeJenkinsfile”

- git push origin master


18- No Browser. Bitbucket.
—————

- Criei um novo “Job”.

- Seleccionei “Pipeline script from SCM” e no SCM “Git”.

- No Repository URL pus: “https://bitbucket.org/oliveirantonio/gradle_basic_demoo”

- E na Credentials escolhi: “oliveirantonio/*******”








