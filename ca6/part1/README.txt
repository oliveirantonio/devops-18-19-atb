PART 0
—————
—————

1 - no TERMINAL. no directório “vagrant-with-ansible-master”.
—————

Fiz correr o comando “vagrant up” no directório, onde está a pasta “vagrant-with-ansible-master”.

Obtive esta informação:
…
…
ansible: Setting up python-ipaddress (1.0.16-1) ...
ansible: Setting up python-pyasn1 (0.1.9-1) ...
ansible: Setting up python-cryptography (1.2.3-1ubuntu0.2) ...
ansible: Setting up ansible (2.8.0-1ppa~xenial) ...
ansible: Processing triggers for libc-bin (2.23-0ubuntu10) ...


2 - no TERMINAL. no directório “vagrant-with-ansible-master”.
————— 

Fiz correr o comando “vagrant ssh ansible”.

Obtive esta informação:
Welcome to Ubuntu 16.04.3 LTS (GNU/Linux 4.4.0-109-generic x86_64)

 * Documentation:  https://help.ubuntu.com
 * Management:     https://landscape.canonical.com
 * Support:        https://ubuntu.com/advantage


3 - no TERMINAL. no directório “vagrant-with-ansible-master”.
————— 

Fiz correr o comando “ansible --version”.

Obtive esta informação:
ansible 2.8.0
  config file = /etc/ansible/ansible.cfg
  configured module search path = [u'/home/vagrant/.ansible/plugins/modules', u'/usr/share/ansible/plugins/modules']
  ansible python module location = /usr/lib/python2.7/dist-packages/ansible
  executable location = /usr/bin/ansible
  python version = 2.7.12 (default, Nov 12 2018, 14:36:49) [GCC 5.4.0 20160609]


4 - no TERMINAL. no directório “vagrant-with-ansible-master”.
————— 

Fiz correr o comando “ping 192.168.33.11”.

Obtive esta informação:
PING 192.168.33.11 (192.168.33.11) 56(84) bytes of data.
64 bytes from 192.168.33.11: icmp_seq=1 ttl=64 time=1.56 ms
64 bytes from 192.168.33.11: icmp_seq=2 ttl=64 time=0.649 ms
64 bytes from 192.168.33.11: icmp_seq=3 ttl=64 time=0.620 ms
64 bytes from 192.168.33.11: icmp_seq=4 ttl=64 time=0.717 ms
64 bytes from 192.168.33.11: icmp_seq=5 ttl=64 time=0.647 ms
64 bytes from 192.168.33.11: icmp_seq=6 ttl=64 time=0.587 ms
64 bytes from 192.168.33.11: icmp_seq=7 ttl=64 time=0.658 ms
64 bytes from 192.168.33.11: icmp_seq=8 ttl=64 time=0.657 ms
64 bytes from 192.168.33.11: icmp_seq=9 ttl=64 time=0.677 ms
64 bytes from 192.168.33.11: icmp_seq=10 ttl=64 time=0.361 ms
64 bytes from 192.168.33.11: icmp_seq=11 ttl=64 time=0.480 ms
64 bytes from 192.168.33.11: icmp_seq=12 ttl=64 time=0.526 ms
64 bytes from 192.168.33.11: icmp_seq=13 ttl=64 time=0.563 ms

Fiz um “ctrl” + “c” para interromper o ping.


5 - no TERMINAL (vagrant). no directório “vagrant-with-ansible-master”.
————— 

Fiz correr o comando “cd /vagrant” e depois um “ls”.

Obtive esta informação:
ansible.cfg  hosts  playbook1.yml  readme.md  Vagrantfile


6 - no TERMINAL (vagrant). no directório “vagrant-with-ansible-master”.
————— 

Fiz correr o comando “ansible host1 -i hosts -m ping”.

Obtive esta informação:
The authenticity of host '192.168.33.11 (192.168.33.11)' can't be established.
ECDSA key fingerprint is SHA256:0oD35/N3xUkvOSKK8sFl0EvSvqCpFgqWmQe77/02OuA.
Are you sure you want to continue connecting (yes/no)?

Fiz correr o opção “yes”.

Obtive esta informação:
host1 | SUCCESS => {
    "ansible_facts": {
        "discovered_interpreter_python": "/usr/bin/python3"
    }, 
    "changed": false, 
    "ping": "pong"
}


7 - no TERMINAL (vagrant). no directório “vagrant-with-ansible-master”.
————— 

Fiz correr o comando “ansible-playbook playbook1.yml”.

Obtive esta informação:
TASK [Gathering Facts] *********************************************************
The authenticity of host '192.168.33.12 (192.168.33.12)' can't be established.
ECDSA key fingerprint is SHA256:0oD35/N3xUkvOSKK8sFl0EvSvqCpFgqWmQe77/02OuA.
Are you sure you want to continue connecting (yes/no)? ok: [host1]

Fiz correr o opção “yes”.

Obtive esta informação:
…
…
TASK [Add group "wildfly"] *****************************************************
changed: [host1]

TASK [Add user "wildfly"] ******************************************************
changed: [host1]

TASK [Change ownership of WildFly installation] ********************************
changed: [host1]

TASK [Enable WildFly to be started at boot] ************************************
 [WARNING]: The service (wildfly) is actually an init script but the system is
managed by systemd

changed: [host1]

PLAY RECAP *********************************************************************
host1                      : ok=17   changed=8    unreachable=0    failed=0    skipped=1    rescued=0    ignored=0   
host2                      : ok=4    changed=0    unreachable=0    failed=0    skipped=0    rescued=0    ignored=0


8 - no Browser (http://localhost:8080).
————— 

Visualizei a página do WildFly “Welcome to WildFly”.


PART 1
—————
—————

