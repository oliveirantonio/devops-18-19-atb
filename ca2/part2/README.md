PART 2 - plantuml-gradle-master
--------

1- No TERMINAL.

- git checkout -b gradle-plantuml
- git push origin gradle-plantuml

2- No TERMINAL. Seguindo o README.md

- ./gradlew renderPlantUml

Obtive esta informação:
BUILD SUCCESSFUL in 1m 22s
1 actionable task: 1 executed

3- No TERMINAL. Seguindo o README.md

- ./gradlew javadoc

Obtive esta informação:
BUILD SUCCESSFUL in 5s
2 actionable tasks: 1 executed, 1 up-to-date

4- No INTELLIJ. no ficheiro “build.gradle” (no directório “src”).

task copyPlantUml(type: Copy) {
    from 'build/puml'
    into 'build/docs/javadoc'
    include ‘**/*.png'}

5- No TERMINAL.

- ./gradlew copyPlantUml

Obtive esta informação:
BUILD SUCCESSFUL in 3s

6- No Browser. Vi as imagem UML no site.

7- No TERMINAL. fiz commit and push “imageswebpage”.

errai-demonstration-gradle-master
--------

8- No INTELLIJ. Projecto “errai-demonstration-gradle-master”.

8.1- No TERMINAL.

- ./gradlew clean

9- No TERMINAL. Para instalar o “Wildfly”.

- ./gradlew provision

informação obtida:
BUILD SUCCESSFUL in 14m 24s
1 actionable task: 1 executed

10- No TERMINAL.

- ./gradlew build

informação obtida:
BUILD FAILED in 1m 54s
5 actionable tasks: 5 executed

11- No INTELLIJ.

- fui ao Gradle Task e fiz Build com sucesso.

12- No TERMINAL.

- git commit -a -m “TaskBuildGradleDone"
- git push origin gradle-plantuml

13- No INTELLIJ.

- Grande redeploy

14- No TERMINAL.

- git commit -a -m "redeployDone"
- git push origin gradle-plantuml

15- No INTELLIJ.

- Instalei no build.gradle o plugin “io.kristiansen.gradle.PlantUMLPlugin”

plugins {
	id 'org.wildfly.build.provision' version '0.0.9'
        id 'io.kristiansen.gradle.PlantUMLPlugin' version '0.0.1'
}

16- No Terminal.

- ./gradlew renderPlantUml
- ./gradlew javadoc

17- No INTELLIJ. no “build.gradle”

task copyPlantUml(type: Copy) {
    from ‘assets’
    into 'build/docs/javadoc'
    include '**/*.png'}

18- No Terminal.

- ./gradlew copyPlantUml

19- No terminal.

- git commit -a -m "gradlewCopyPlantUml"
- git push origin gradle-plantuml

20- No INTELLIJ. na path: build/docs/javadoc/overview-summary.hmtl

<h2>Class Diagram</h2>
<img src="ClassDiagram.png"></a><br/>

<h2>Sequence Diagram</h2>
<img src="SequenceDiagram.png"></a><br/>

21- No terminal.

- git commit -a -m “browserWithImages”
- git push origin gradle-plantuml

22- No TERMINAL.

- git add -A
- git status
- git commit -a -m “browserWithImages”
- git push origin gradle-plantuml
- git checkout master
- git pull origin master
- git merge gradle-plantuml
- git push origin master

23- No TERMINAL.

- git tag -a ca2part2 -m "ca2-part2”
- git push origin ca2part2









